# OpenML dataset: Wine-Dataset

https://www.openml.org/d/43612

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

These data are the results of a chemical analysis of wines grown in the same region in Italy but derived from three different cultivars. The analysis determined the quantities of 13 constituents found in each of the three types of wines.
Number of Instances: 178
Number of Attributes: 13
Associated Tasks: Classification
Source : http://archive.ics.uci.edu/ml/datasets/Wine

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43612) of an [OpenML dataset](https://www.openml.org/d/43612). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43612/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43612/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43612/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

